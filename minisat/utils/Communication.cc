/*
 * Hitoshi Togasaki
 */

#include "utils/Communication.h"
#include "core/Solver.h"
#include <vector>
#include <algorithm>
#include "utils/System.h"


//Master
//==================================================================================================================================================================================================================================================================-
char GetStats(int node){
	MPI_Status status;
	int tag = 0;
	char recv_msg;
	char send_msg = 'P';
	MPI_Send(&send_msg, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);
	MPI_Recv(&recv_msg, 1, MPI_CHAR, node, tag, MPI_COMM_WORLD, &status);
	switch (recv_msg) {
		case 'F'://find a solution
			return 'F';
			break;
		case 'N'://not find
			return 'N';
			break;
		default:
			break;
	}

	return 'N';
}

void TerminateAllWorker(int num_proc){
	//std::cout << "TerminateAllWorker  " << std::endl;
	int tag = 0;
	char send_msg = 'T';
	for (int node = 1; node < num_proc; node++){
		MPI_Send(&send_msg, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);
	}
	return ;
}
void TerminateWorker(int node){
	int tag = 0;
	char send_msg = 'T';
	MPI_Send(&send_msg, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);
}
void GetResult(int node, Solver* _solver, bool win){
	MPI_Status status;
	int tag = 0;

	char send_msg = win ? 'W' : 'L' ;
	MPI_Send(&send_msg, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);
	char ans;
	if (win){
		MPI_Recv(&ans, 1, MPI_CHAR, node, tag, MPI_COMM_WORLD, &status);
		_solver->solution_status = (ans == 'S' ? 0 : 1);
	}
	MPI_Recv(&_solver->m_starts[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_conflicts[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_sum_learnt_size[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_decisions[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_propagations[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);

	MPI_Recv(&_solver->m_tot_literals[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_max_literals[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);

	MPI_Recv(&_solver->m_w_num_effective_clause[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_sum_hit[node], 1, MPI_UNSIGNED, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_clause_table_size[node], 1, MPI_UNSIGNED, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_mem_used[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	//parallel statistics
	MPI_Recv(&_solver->m_w_all_imp_clause[node], 1, MPI_UNSIGNED_LONG, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_imp_clause[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_exp_clause[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);


	//communication

	MPI_Recv(&_solver->m_w_search_lock_time[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_put_cla_time[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_recv_cla_time[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_imp_from_que_time[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);
	//special bump
	MPI_Recv(&_solver->m_w_sp_bump_restart_time[node], 1, MPI_DOUBLE, node, tag, MPI_COMM_WORLD, &status);

	MPI_Recv(&_solver->m_w_dynamic_start[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	//reduce db
	MPI_Recv(&_solver->m_w_reduceDb_cnt[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);

	MPI_Recv(&_solver->m_w_original_cla_use_cnt[node], 1, MPI_UINT64_T, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_own_cla_use_cnt[node], 1, MPI_UINT64_T, node, tag, MPI_COMM_WORLD, &status);
	MPI_Recv(&_solver->m_w_imp_cla_use_cnt[node], 1, MPI_UINT64_T, node, tag, MPI_COMM_WORLD, &status);

	for (int i = 0; i < 5; i++){
		MPI_Recv(&_solver->m_w_rm_imp_cla_use_cnt[node][i], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	}
	MPI_Recv(&_solver->m_w_rm_other_imp_cla_use_cnt[node], 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	return ;
}
void CommunicateWithWorker(int num_proc, double real_lim, Solver *_solver){
	bool terminate = false;
	int num_termination = 0;

	while (true){
		EraseImportMasterDb(_solver);
		for (int worker = 1; worker < num_proc; worker++){
			double time = _solver->realTime();
			if (time >=  real_lim){
				terminate = true;
			}
			if (terminate){
				GetResult(worker, _solver, false);//loser
				TerminateWorker(worker);
				num_termination++;
				if (num_termination >= num_proc - 1)break;
				continue;
			}
			char stats = GetStats(worker);
			switch (stats) {
			case 'F'://find a solution
				_solver->winner_node = worker;
				GetResult(worker, _solver, true);//winner
				terminate = true;
				TerminateWorker(worker);
				num_termination++;
				break;
			case 'N'://not a solution
				GetLearntClause(worker, _solver);
				if (_solver->imp_learnt_clause){
					SendLearntClause(worker, _solver);
				}
				break;
			default:
				break;
			}
		}
		if (num_termination >= num_proc - 1)break;
	}

	return ;
}
bool ClauseInDb(int node, const std::vector<int> &clause, Solver* _solver){
	if (_solver->check_dup_clause.find(clause) != _solver->check_dup_clause.end()){// find
		_solver->duplicated_ratio[node]++;
		return true;
	}
	return false;//not find
}
void GetLearntClause(int node, Solver* _solver){
	MPI_Status status;
	int tag = 0;
	char send_message = 'C';
	MPI_Send(&send_message, 1, MPI_CHAR, node, tag, MPI_COMM_WORLD);
	int imp_num;
	MPI_Recv(&imp_num, 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	_solver->m_sum_getlearntclause[node] += imp_num;

	for (int i = 0; i < imp_num; i++){
		int clause_size;
		MPI_Recv(&clause_size, 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
		int *buffer_clause = new int[clause_size];
		double elapsed_time;
		MPI_Recv(buffer_clause, clause_size, MPI_INT,  node, tag, MPI_COMM_WORLD, &status);
		MPI_Recv(&elapsed_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD, &status);
		std::vector<int> clause;
		clause.resize(clause_size);
		for (int j = 0; j < clause_size; j++){
			clause[j] = buffer_clause[j];
		}

		delete[] buffer_clause;
		_solver->import_learnt_master_db[node].emplace_back(clause);
		for (int j = 1; j < _solver->num_procs; j++){
			if (j == node)continue;
			_solver->import_learnt_db_tail[j][node]++;
		}
	}
	return ;
}

void SendLearntClause(int node, Solver* _solver){
	char send_message = 'S';
	int tag = 0;
	MPI_Send(&send_message, 1, MPI_CHAR, node, tag, MPI_COMM_WORLD);
	std::queue<std::vector<int>> que;
	for (int i = 1; i < _solver->num_procs; i++){
		if (i == node)continue;
		int &head = _solver->import_learnt_db_head[node][i];
		int &tail = _solver->import_learnt_db_tail[node][i];
		while (head < tail){
			std::vector<int> &tmp = _solver->import_learnt_master_db[i][head];
			que.push(tmp);
			head++;
		}
	}
	int snd_size = que.size();
	MPI_Send(&snd_size, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	while (!que.empty()){
		std::vector<int> &tmp = que.front();
		int clause_size = tmp.size();
		MPI_Send(&clause_size, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
		int* buffer_clause = new int[clause_size];
		for (int j = 0; j < clause_size; j++){
			buffer_clause[j] = tmp[j];
		}
		MPI_Send(buffer_clause, clause_size, MPI_INT,  node, tag, MPI_COMM_WORLD);

		que.pop();
		delete[] buffer_clause;
	}
	_solver->m_exp_clause += snd_size;
	return ;
}

void EraseImportMasterDb(Solver* _solver){
	//erase
	for (int i = 1; i < _solver->num_procs; i++){//erase
		int head_min = 1 << 30;
		for (int j = 1; j < _solver->num_procs; j++){
			if (j == i)continue;
			head_min = std::min(head_min, _solver->import_learnt_db_head[j][i]);
		}
		if (head_min < (1 << 30)){
			_solver->import_learnt_master_db[i].erase(_solver->import_learnt_master_db[i].begin(), _solver->import_learnt_master_db[i].begin() + head_min);
			_solver->import_learnt_master_db[i].shrink_to_fit();
			for (int j = 1; j < _solver->num_procs; j++){
				if (j == i)continue;
				int &head = _solver->import_learnt_db_head[j][i];
				int &tail = _solver->import_learnt_db_tail[j][i];
				head -= head_min;
				tail -= head_min;
				//assert(head >= 0 && tail >= 0);
			}
			_solver->m_erase_master_db_num += head_min;
		}
	}
	_solver->import_learnt_master_db.shrink_to_fit();
	return ;
}


//==================================================================================================================================================================================================================================================================


//Worker
//==================================================================================================================================================================================================================================================================
char RecvMsg(int node){
	char recv_msg;
	int tag;
	MPI_Status status;
	MPI_Recv(&recv_msg, 1, MPI_CHAR, node, tag, MPI_COMM_WORLD, &status);
	return recv_msg;
}

void SendMsg(int node, char msg){
	char send_msg = msg;
	int tag = 0;
	MPI_Send(&send_msg, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);

	return;
}

void RecvLearntClause(int node, Solver* _solver){
	int tag = 0;
	MPI_Status status;
	int imp_num;
	MPI_Recv(&imp_num, 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
	for (int i = 0; i < imp_num; i++){
		int clause_size;
		MPI_Recv(&clause_size, 1, MPI_INT, node, tag, MPI_COMM_WORLD, &status);
		int *buffer_clause = new int[clause_size];
		MPI_Recv(buffer_clause, clause_size, MPI_INT,  node, tag, MPI_COMM_WORLD, &status);
		std::vector<int> clause;
		clause.resize(clause_size);
		for (int j = 0; j < clause_size; j++){
			clause[j] = buffer_clause[j];
		}
		delete[] buffer_clause;
		double start = _solver->realTime();
		pthread_mutex_lock(&_solver->mutex);
		double end = _solver->realTime();
		_solver->w_import_learnt_que.push(clause);
		_solver->w_recv_cla_time += (end - start);
		pthread_mutex_unlock(&_solver->mutex);
	}
	//std::cout << "RecvLearntClause End!!!!" << std::endl;
	return ;
}
void PutLearntClause(int node, Solver* _solver){//worker → master
	int tag = 0;
	double start = _solver->realTime();
	pthread_mutex_lock(&_solver->mutex);
	double end = _solver->realTime();
	_solver->w_put_cla_time += (end - start);
	std::queue<std::vector<int>> &que = _solver->export_learnt_worker_db;
	int put_num = que.size();
	MPI_Send(&put_num, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	for (int i = 0; i < put_num; i++){

		std::vector<int> &tmp_clause = que.front();
		double &elapsed_time = _solver->ts_learnt_clause.front();

		int clause_size = tmp_clause.size();
		MPI_Send(&clause_size, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
		int* buffer_clause = new int[clause_size];
		for (int j = 0; j < clause_size; j++){
			buffer_clause[j] = tmp_clause[j];
		}
		MPI_Send(buffer_clause, clause_size, MPI_INT,  node, tag, MPI_COMM_WORLD);
		MPI_Send(&elapsed_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
		que.pop();
		_solver->ts_learnt_clause.pop();
		delete[] buffer_clause;
	}
	pthread_mutex_unlock(&_solver->mutex);

	return ;
}
void SendResult(int node, Solver* _solver, bool win){
	MPI_Status status;
	int tag = 0;
	if (win){
		char ans;
		ans = (_solver->solution_status == 0 ? 'S' : 'U');
		/*
		 * S : SATISFIABLE
		 * U: UNSATISFIABLE
		 */
		MPI_Send(&ans, 1, MPI_CHAR,  node, tag, MPI_COMM_WORLD);
	}
	MPI_Send(&_solver->starts, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->conflicts, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_sum_learnt_size, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->decisions, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->propagations, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);

	MPI_Send(&_solver->tot_literals, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->max_literals, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);

	MPI_Send(&_solver->w_num_effective_clause, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_sum_hit, 1, MPI_UNSIGNED,  node, tag, MPI_COMM_WORLD);
	uint32_t w_clause_table_size = _solver->w_clause_table.size();
	MPI_Send(&w_clause_table_size, 1, MPI_UNSIGNED,  node, tag, MPI_COMM_WORLD);

	double mem_used = memUsedPeak();
	MPI_Send(&mem_used, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	//parallel
	MPI_Send(&_solver->w_all_imp_clause, 1, MPI_UNSIGNED_LONG,  node, tag, MPI_COMM_WORLD);

	MPI_Send(&_solver->w_imp_clause, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_exp_clause, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	//communication time
	MPI_Send(&_solver->w_search_lock_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_put_cla_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_recv_cla_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_imp_from_que_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_sp_bump_restart_time, 1, MPI_DOUBLE,  node, tag, MPI_COMM_WORLD);

	MPI_Send(&_solver->w_dynamic_start, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	//reduceDb_cnt
	MPI_Send(&_solver->w_reduceDb_cnt, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);

	MPI_Send(&_solver->w_original_cla_use_cnt, 1, MPI_UINT64_T,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_own_cla_use_cnt, 1, MPI_UINT64_T,  node, tag, MPI_COMM_WORLD);
	MPI_Send(&_solver->w_imp_cla_use_cnt, 1, MPI_UINT64_T,  node, tag, MPI_COMM_WORLD);

	//
	for (int i = 0; i < 5; i++){
		MPI_Send(&_solver->w_rm_imp_cla_use_cnt[i], 1, MPI_INT,  node, tag, MPI_COMM_WORLD);
	}
	MPI_Send(&_solver->w_rm_other_imp_cla_use_cnt, 1, MPI_INT,  node, tag, MPI_COMM_WORLD);

	return ;
}




