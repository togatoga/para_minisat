/*Hitoshi Togasaki
 *
 */
#include "mpi.h"
#include "core/Solver.h"

using namespace Minisat;
/*
 *Master
 */

void CommunicateWithWorker(int num_proc, double real_lim, Solver* _solver);
char GetStats(int node, Solver* _solver);
//void GetWinnerResult(int node, Solver* _solver);//get winner result
//void GetLoserResult(int node, Solver* _solver);//get loser result
void GetResult(int node, Solver* _solver, bool win);
void GetLearntClause(int node, Solver* _solver);
void SendLearntClause(int node,  Solver* _solver);
void TerminateAllWorker(Solver* _solver);
void TerminateWorker(int node);
bool ClauseInDb(int node, const std::vector<int> &clause, Solver* _solver);
void EraseImportMasterDb(Solver* _solver);
void EraseMasterTable(Solver* _solver);

/*
 * Worker
 */

char RecvMsg(int node);
//void *CommunicateWithMaster(Solver* _solver);
bool CheckFindASolution(Solver* _solver);
void SendMsg(int node, char msg);
void PutLearntClause(int node, Solver* _solver);
//void SendWinnerResult(int node, Solver* _solver);//send statistics winner result
//void SendLoserResult(int node, Solver* _solver);//send statistics loser result
void SendResult(int node, Solver* _solver, bool win);
void RecvLearntClause(int node, Solver* _solver);
