/***************************************************************************************[Solver.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007-2010, Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**************************************************************************************************/

#include <math.h>

#include "mtl/Sort.h"
#include "core/Solver.h"




//add
#include "mpi.h"
#include <algorithm>
#include <iostream>
#include <queue>
using namespace Minisat;

//=================================================================================================
// Options:


static const char* _cat = "CORE";

static DoubleOption  opt_var_decay         (_cat, "var-decay",   "The variable activity decay factor",            0.95,     DoubleRange(0, false, 1, false));
static DoubleOption  opt_clause_decay      (_cat, "cla-decay",   "The clause activity decay factor",              0.999,    DoubleRange(0, false, 1, false));
static DoubleOption  opt_random_var_freq   (_cat, "rnd-freq",    "The frequency with which the decision heuristic tries to choose a random variable", 0, DoubleRange(0, true, 1, true));
static DoubleOption  opt_random_seed       (_cat, "rnd-seed",    "Used by the random variable selection",         91648253, DoubleRange(0, false, HUGE_VAL, false));
static IntOption     opt_ccmin_mode        (_cat, "ccmin-mode",  "Controls conflict clause minimization (0=none, 1=basic, 2=deep)", 2, IntRange(0, 2));
static IntOption     opt_phase_saving      (_cat, "phase-saving", "Controls the level of phase saving (0=none, 1=limited, 2=full)", 2, IntRange(0, 2));
static BoolOption    opt_rnd_init_act      (_cat, "rnd-init",    "Randomize the initial activity", true);
static BoolOption    opt_luby_restart      (_cat, "luby",        "Use the Luby restart sequence", true);
static IntOption     opt_restart_first     (_cat, "rfirst",      "The base restart interval", 100, IntRange(1, INT32_MAX));
static DoubleOption  opt_restart_inc       (_cat, "rinc",        "Restart interval increase factor", 2, DoubleRange(1, false, HUGE_VAL, false));
static DoubleOption  opt_garbage_frac      (_cat, "gc-frac",     "The fraction of wasted memory allowed before a garbage collection is triggered",  0.20, DoubleRange(0, false, HUGE_VAL, false));


//togasaki
//parallel
static IntOption     opt_share_limit_size     (_cat, "share-lim-size",      "The share learnt size limit ( import <= share-lim-size)", 4, IntRange(2, INT32_MAX));
static IntOption     opt_effective_limit_size     (_cat, "eff-lim-size",      "The effective learnt size limit", 4, IntRange(2, INT32_MAX));
static IntOption     opt_sp_bump_interval     (_cat, "sp-bump-interval",      "The share learnt size limit ( import <= share-lim-size)", 3, IntRange(1, INT32_MAX));
static BoolOption    opt_imp_learnt_clause      (_cat, "imp-cla",    "Import learnt clause from queue", true);
static DoubleOption opt_hit_rate (_cat, "hit-rate", "The constant used to force restart", 0.05, DoubleRange(0, false, 1, false));
static BoolOption    opt_hit_restart      (_cat, "hit-restart",    "Clause hitting restart", true);

//linear erase
static IntOption     opt_linear_reduce_db     (_cat, "linearReduceDB",      "Controls linear reduce DB",  true);
static IntOption     opt_first_reduce_db     (_cat, "firstReduceDB",      "The number of conflicts before the first reduce DB", 2000, IntRange(0, INT32_MAX));
static IntOption    opt_inc_reduce_db     (_cat, "incReduceDB",      "Increment for reduce DB", 300, IntRange(0, INT32_MAX));

//=================================================================================================
// Constructor/Destructor:


Solver::Solver() :

    // Parameters (user settable):
    //
    verbosity        (0)
  , var_decay        (opt_var_decay)
  , clause_decay     (opt_clause_decay)
  , random_var_freq  (opt_random_var_freq)
  , random_seed      (opt_random_seed)
  , luby_restart     (opt_luby_restart)
  , ccmin_mode       (opt_ccmin_mode)
  , phase_saving     (opt_phase_saving)
  , rnd_pol          (false)
  , rnd_init_act     (opt_rnd_init_act)
  , garbage_frac     (opt_garbage_frac)
  , restart_first    (opt_restart_first)
  , restart_inc      (opt_restart_inc)

    // Parameters (the rest):
    //
  , learntsize_factor((double)1/(double)3), learntsize_inc(1.1)

    // Parameters (experimental):
    //
  , learntsize_adjust_start_confl (100)
  , learntsize_adjust_inc         (1.5)

    // Statistics: (formerly in 'SolverStats')
    //
  , solves(0), starts(0), decisions(0), rnd_decisions(0), propagations(0), conflicts(0)
  , dec_vars(0), clauses_literals(0), learnts_literals(0), max_literals(0), tot_literals(0)

  , ok                 (true)
  , cla_inc            (1)
  , var_inc            (1)
  , watches            (WatcherDeleted(ca))
  , qhead              (0)
  , simpDB_assigns     (-1)
  , simpDB_props       (0)
  , order_heap         (VarOrderLt(activity))
  , progress_estimate  (0)
  , remove_satisfied   (true)

    // Resource constraints:
    //
  , conflict_budget    (-1)
  , propagation_budget (-1)
  , asynch_interrupt   (false)
	//togasaki solver

	//togasaki statistics
	,w_num_effective_clause(0)
	,w_imp_clause(0)
	,w_exp_clause(0)
	,w_all_imp_clause(0)
	,w_reduceDb_cnt(0)

	,w_sum_hit(0)
	,w_put_cla_time(0)
    ,w_recv_cla_time(0)
	,w_imp_from_que_time(0)
	,w_search_lock_time(0)

	//dynamic restart
	,w_hit_restart(opt_hit_restart)
	,effective_limit_size(opt_effective_limit_size)
	,w_share_limit_size(opt_share_limit_size)
	,w_dynamic_start(0)
	,w_hit_rate(opt_hit_rate)

	//togasaki
	,solution_status(-1)
	,find_solution(false)
	,terminate_search(false)
	,winner_node(-1)
	,imp_learnt_clause(opt_imp_learnt_clause)

	,m_imp_clause(0)
	,m_exp_clause(0)

	,m_imp_time(0)
	,m_exp_time(0)
	,m_erase_master_db_num(0)
	//bump specail
	, w_sp_order_heap         (SP_VarOrderLt(w_sp_activity))
	,w_sp_var_inc(1.0)
	,w_sp_var_decay(0.95)
	,sp_bump_interval(opt_sp_bump_interval)
	,w_sp_bump_restart_time(0)
	//LBD
	,FLAG_LBD(0)

	//counter
	,w_original_cla_use_cnt(0)
	,w_own_cla_use_cnt(0)
	,w_imp_cla_use_cnt(0)
	,w_unused_imp_cla_use_cnt(0)

	//togasaki
	,firstReduceDB(opt_first_reduce_db)
	,incReduceDB(opt_inc_reduce_db)
	,curRestart(1)
    ,w_linear_reduce_db(opt_linear_reduce_db)
{
	w_sum_learnt_size = 0;
	real_time = std::chrono::system_clock::now() ;
	w_rm_imp_cla_use_cnt.growTo(5);

	//linear
	incReduceDB += 2 * my_id;
	firstReduceDB += my_id;
	nbclausesbeforereduce = firstReduceDB;
}


Solver::~Solver()
{
}

//parallel
void Solver::initParallelStatus(){
	duplicated_ratio = new double[num_procs];
	m_starts = new uint32_t[num_procs];
	m_conflicts = new uint32_t[num_procs];
	m_decisions = new uint32_t[num_procs];
	m_propagations = new uint32_t[num_procs];
	m_tot_literals = new uint32_t[num_procs];
	m_max_literals = new uint32_t[num_procs];
	m_sum_getlearntclause = new uint32_t[num_procs];
	//staticstics
	m_w_all_imp_clause = new uint64_t[num_procs];
	m_w_imp_clause = new uint32_t[num_procs];
	m_w_exp_clause = new uint32_t[num_procs];
	m_w_search_lock_time = new double[num_procs];
	m_w_put_cla_time = new double[num_procs];
	m_w_recv_cla_time = new double[num_procs];
	m_w_imp_from_que_time = new double[num_procs];
	m_w_sum_hit = new uint32_t[num_procs];
	m_w_mem_used = new double[num_procs];
	m_w_reduceDb_cnt = new uint32_t[num_procs];

	m_w_num_effective_clause = new uint32_t[num_procs];
	m_w_sum_learnt_size = new double[num_procs];
	m_w_dynamic_start = new uint32_t[num_procs];
	m_w_clause_table_size = new uint32_t[num_procs];
	//specialbump
	m_w_sp_bump_restart_time = new double[num_procs];

	//use count
	m_w_original_cla_use_cnt = new uint64_t[num_procs];
	m_w_own_cla_use_cnt = new uint64_t[num_procs];
	m_w_imp_cla_use_cnt = new uint64_t[num_procs];

	m_w_rm_imp_cla_use_cnt = new uint32_t*[num_procs];
	for (int i = 0; i < num_procs; i++){
	  m_w_rm_imp_cla_use_cnt[i] = new uint32_t[5];
	}
	//fill 0 special
	for (int i = 0; i < num_procs; i++){
		for (int j = 0; j < 5; j++){
			m_w_rm_imp_cla_use_cnt[i][j] = 0;
		}
	}
	m_w_rm_other_imp_cla_use_cnt = new uint32_t[num_procs];


	import_learnt_master_db.resize(num_procs);
    import_learnt_db_head = new int*[num_procs];
    import_learnt_db_tail = new int*[num_procs];

	for (int i = 0; i < num_procs; i++){
		import_learnt_db_head[i] = new int[num_procs];
		import_learnt_db_tail[i] = new int[num_procs];
	}

	//fill 0
	for (int i = 0; i < num_procs; i++){
		duplicated_ratio[i] = 0;
		m_starts[i] = 0;
		m_conflicts[i] = 0;
		m_decisions[i] = 0;
		m_propagations[i] = 0;
		m_tot_literals[i] = 0;
		m_max_literals[i] = 0;
		m_sum_getlearntclause[i] = 0;

		m_w_all_imp_clause[i] = 0;
		m_w_imp_clause[i] = 0;
		m_w_exp_clause[i] = 0;

		m_w_put_cla_time[i] = 0;
		m_w_recv_cla_time[i] = 0;
		m_w_imp_from_que_time[i] = 0;
		m_w_search_lock_time[i] = 0;
		m_w_num_effective_clause[i] = 0;
		m_w_sum_learnt_size[i] = 0;
		m_w_dynamic_start[i] = 0;
		m_w_sum_hit[i] = 0;
		m_w_clause_table_size[i] = 0;
		m_w_mem_used[i] = 0;
		//specialbump
		m_w_sp_bump_restart_time[i] = 0;
		m_w_reduceDb_cnt[i] = 0;


		//use learnt clause
		   m_w_original_cla_use_cnt[i] = 0;
		   m_w_own_cla_use_cnt[i] = 0;
		   m_w_imp_cla_use_cnt[i] = 0;

		   m_w_rm_other_imp_cla_use_cnt[i] = 0;
	}

	for (int i = 0; i <  num_procs; i++){
		for (int j = 0; j < num_procs; j++){
			import_learnt_db_head[i][j] = 0;
			import_learnt_db_tail[i][j] = 0;
		}
	}
	m_import_learnt_master_db.resize(num_procs);


    return ;
}
//=================================================================================================
// Minor methods:


// Creates a new SAT variable in the solver. If 'decision' is cleared, variable will not be
// used as a decision variable (NOTE! This has effects on the meaning of a SATISFIABLE result).
//
Var Solver::newVar(bool sign, bool dvar)
{
    int v = nVars();
    watches  .init(mkLit(v, false));
    watches  .init(mkLit(v, true ));
    assigns  .push(l_Undef);
    vardata  .push(mkVarData(CRef_Undef, 0));
    //activity .push(0);
    activity .push(rnd_init_act ? drand(random_seed) * 0.00001 : 0);
    //togasaki
    w_sp_activity.push(0);
    //LBD
    FLAG_diff.push(0);
    seen     .push(0);
    polarity .push(sign);
    decision .push();
    trail    .capacity(v+1);
    setDecisionVar(v, dvar);
    return v;
}


bool Solver::addClause_(vec<Lit>& ps)
{
    assert(decisionLevel() == 0);
    if (!ok) return false;

    // Check if clause is satisfied and remove false/duplicate literals:
    sort(ps);
    Lit p; int i, j;
    for (i = j = 0, p = lit_Undef; i < ps.size(); i++)
        if (value(ps[i]) == l_True || ps[i] == ~p)
            return true;
        else if (value(ps[i]) != l_False && ps[i] != p)
            ps[j++] = p = ps[i];
    ps.shrink(i - j);

    if (ps.size() == 0)
        return ok = false;
    else if (ps.size() == 1){
    	uncheckedEnqueue(ps[0]);
        return ok = (propagate() == CRef_Undef);
    }else{
        CRef cr = ca.alloc(ps, false);
        clauses.push(cr);
        attachClause(cr);
    }

    return true;
}


void Solver::attachClause(CRef cr) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    watches[~c[0]].push(Watcher(cr, c[1]));
    watches[~c[1]].push(Watcher(cr, c[0]));
    if (c.learnt()) learnts_literals += c.size();
    else            clauses_literals += c.size(); }


void Solver::detachClause(CRef cr, bool strict) {
    const Clause& c = ca[cr];
    assert(c.size() > 1);
    
    if (strict){
        remove(watches[~c[0]], Watcher(cr, c[1]));
        remove(watches[~c[1]], Watcher(cr, c[0]));
    }else{
        // Lazy detaching: (NOTE! Must clean all watcher lists before garbage collecting this clause)
        watches.smudge(~c[0]);
        watches.smudge(~c[1]);
    }

    if (c.learnt()) learnts_literals -= c.size();
    else            clauses_literals -= c.size(); }


void Solver::removeClause(CRef cr) {
    Clause& c = ca[cr];
    //togasaki
    if (c.getImported() || c.getExported()){
    	std::vector<int> tmp;
    	tmp.resize(c.size());
    	for (int i = 0; i < c.size(); i++){
    		tmp[i] = c[i].x;
    	}
    	std::sort(tmp.begin(), tmp.end());
    	w_clause_table.erase(tmp);
    }

    //togasaki
    detachClause(cr);
    // Don't leave pointers to free'd memory!
    if (locked(c)) vardata[var(c[0])].reason = CRef_Undef;
    c.mark(1); 
    ca.free(cr);
}
bool Solver::satisfied(const std::vector<int>& c) const {
    for (int i = 0; i < c.size(); i++){
        if (value(toLit(c[i])) == l_True){
            return true;
        }
    }
    return false;
}

bool Solver::satisfied(const Clause& c) const {
    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) == l_True)
            return true;
    return false; }


// Revert to the state at given level (keeping all assignment at 'level' but not beyond).
//
void Solver::cancelUntil(int level) {
    if (decisionLevel() > level){
        for (int c = trail.size()-1; c >= trail_lim[level]; c--){
            Var      x  = var(trail[c]);
            assigns [x] = l_Undef;
            if (phase_saving > 1 || (phase_saving == 1) && c > trail_lim.last())
                polarity[x] = sign(trail[c]);
            insertVarOrder(x); }
        qhead = trail_lim[level];
        trail.shrink(trail.size() - trail_lim[level]);
        trail_lim.shrink(trail_lim.size() - level);
    } }


//=================================================================================================
// Major methods:


Lit Solver::pickBranchLit()
{
    Var next = var_Undef;

    // Random decision:
    if (drand(random_seed) < random_var_freq && !order_heap.empty()){
        next = order_heap[irand(random_seed,order_heap.size())];
        if (value(next) == l_Undef && decision[next])
            rnd_decisions++; }

    // Activity based decision:
    while (next == var_Undef || value(next) != l_Undef || !decision[next])
        if (order_heap.empty()){
            next = var_Undef;
            break;
        }else
            next = order_heap.removeMin();

    return next == var_Undef ? lit_Undef : mkLit(next, rnd_pol ? drand(random_seed) < 0.5 : polarity[next]);
}


/*_________________________________________________________________________________________________
|
|  analyze : (confl : Clause*) (out_learnt : vec<Lit>&) (out_btlevel : int&)  ->  [void]
|  
|  Description:
|    Analyze conflict and produce a reason clause.
|  
|    Pre-conditions:
|      * 'out_learnt' is assumed to be cleared.
|      * Current decision level must be greater than root level.
|  
|    Post-conditions:
|      * 'out_learnt[0]' is the asserting literal at level 'out_btlevel'.
|      * If out_learnt.size() > 1 then 'out_learnt[1]' has the greatest decision level of the 
|        rest of literals. There may be others from the same level though.
|  
|________________________________________________________________________________________________@*/
void Solver::analyze(CRef confl, vec<Lit>& out_learnt, int& out_btlevel)
{
    int pathC = 0;
    Lit p     = lit_Undef;

    // Generate conflict clause:
    //
    out_learnt.push();      // (leave room for the asserting literal)
    int index   = trail.size() - 1;

    do{
        assert(confl != CRef_Undef); // (otherwise should be UIP)
        Clause& c = ca[confl];

        if (c.learnt()){
            claBumpActivity(c);
        }
        /*@@prompt to Update LBD score*/
        if(c.learnt()  && c.lbd()>2) {
        	unsigned int nblevels = computeLBD(c);
        	if(nblevels+1<c.lbd() ) { // improve the LBD
        		// seems to be interesting : keep it for the next round
        		c.setLBD(nblevels); // Update it
        		//if (nblevels <= 2)num_lbd2++;
        	}
        }
        /*##prompt to Update LBD score*/

        for (int j = (p == lit_Undef) ? 0 : 1; j < c.size(); j++){
            Lit q = c[j];

            if (!seen[var(q)] && level(var(q)) > 0){
                varBumpActivity(var(q));
                seen[var(q)] = 1;
                if (level(var(q)) >= decisionLevel())
                    pathC++;
                else
                    out_learnt.push(q);
            }
        }
        
        // Select next clause to look at:
        while (!seen[var(trail[index--])]);
        p     = trail[index+1];
        confl = reason(var(p));
        seen[var(p)] = 0;
        pathC--;

    }while (pathC > 0);
    out_learnt[0] = ~p;

    // Simplify conflict clause:
    //
    int i, j;
    out_learnt.copyTo(analyze_toclear);
    if (ccmin_mode == 2){
        uint32_t abstract_level = 0;
        for (i = 1; i < out_learnt.size(); i++)
            abstract_level |= abstractLevel(var(out_learnt[i])); // (maintain an abstraction of levels involved in conflict)

        for (i = j = 1; i < out_learnt.size(); i++)
            if (reason(var(out_learnt[i])) == CRef_Undef || !litRedundant(out_learnt[i], abstract_level))
                out_learnt[j++] = out_learnt[i];
        
    }else if (ccmin_mode == 1){
        for (i = j = 1; i < out_learnt.size(); i++){
            Var x = var(out_learnt[i]);

            if (reason(x) == CRef_Undef)
                out_learnt[j++] = out_learnt[i];
            else{
                Clause& c = ca[reason(var(out_learnt[i]))];
                for (int k = 1; k < c.size(); k++)
                    if (!seen[var(c[k])] && level(var(c[k])) > 0){
                        out_learnt[j++] = out_learnt[i];
                        break; }
            }
        }
    }else
        i = j = out_learnt.size();

    max_literals += out_learnt.size();
    out_learnt.shrink(i - j);
    tot_literals += out_learnt.size();

    // Find correct backtrack level:
    //
    if (out_learnt.size() == 1)
        out_btlevel = 0;
    else{
        int max_i = 1;
        // Find the first literal assigned at the next-highest level:
        for (int i = 2; i < out_learnt.size(); i++)
            if (level(var(out_learnt[i])) > level(var(out_learnt[max_i])))
                max_i = i;
        // Swap-in this literal at index 1:
        Lit p             = out_learnt[max_i];
        out_learnt[max_i] = out_learnt[1];
        out_learnt[1]     = p;
        out_btlevel       = level(var(p));
    }

    for (int j = 0; j < analyze_toclear.size(); j++) seen[var(analyze_toclear[j])] = 0;    // ('seen[]' is now cleared)
}


// Check if 'p' can be removed. 'abstract_levels' is used to abort early if the algorithm is
// visiting literals at levels that cannot be removed later.
bool Solver::litRedundant(Lit p, uint32_t abstract_levels)
{
    analyze_stack.clear(); analyze_stack.push(p);
    int top = analyze_toclear.size();
    while (analyze_stack.size() > 0){
        assert(reason(var(analyze_stack.last())) != CRef_Undef);
        Clause& c = ca[reason(var(analyze_stack.last()))]; analyze_stack.pop();

        for (int i = 1; i < c.size(); i++){
            Lit p  = c[i];
            if (!seen[var(p)] && level(var(p)) > 0){
                if (reason(var(p)) != CRef_Undef && (abstractLevel(var(p)) & abstract_levels) != 0){
                    seen[var(p)] = 1;
                    analyze_stack.push(p);
                    analyze_toclear.push(p);
                }else{
                    for (int j = top; j < analyze_toclear.size(); j++)
                        seen[var(analyze_toclear[j])] = 0;
                    analyze_toclear.shrink(analyze_toclear.size() - top);
                    return false;
                }
            }
        }
    }

    return true;
}


/*_________________________________________________________________________________________________
|
|  analyzeFinal : (p : Lit)  ->  [void]
|  
|  Description:
|    Specialized analysis procedure to express the final conflict in terms of assumptions.
|    Calculates the (possibly empty) set of assumptions that led to the assignment of 'p', and
|    stores the result in 'out_conflict'.
|________________________________________________________________________________________________@*/
void Solver::analyzeFinal(Lit p, vec<Lit>& out_conflict)
{
    out_conflict.clear();
    out_conflict.push(p);

    if (decisionLevel() == 0)
        return;

    seen[var(p)] = 1;

    for (int i = trail.size()-1; i >= trail_lim[0]; i--){
        Var x = var(trail[i]);
        if (seen[x]){
            if (reason(x) == CRef_Undef){
                assert(level(x) > 0);
                out_conflict.push(~trail[i]);
            }else{
                Clause& c = ca[reason(x)];
                for (int j = 1; j < c.size(); j++)
                    if (level(var(c[j])) > 0)
                        seen[var(c[j])] = 1;
            }
            seen[x] = 0;
        }
    }

    seen[var(p)] = 0;
}


void Solver::uncheckedEnqueue(Lit p, CRef from)
{
    assert(value(p) == l_Undef);
    assigns[var(p)] = lbool(!sign(p));
    vardata[var(p)] = mkVarData(from, decisionLevel());
    trail.push_(p);
}


/*_________________________________________________________________________________________________
|
|  propagate : [void]  ->  [Clause*]
|  
|  Description:
|    Propagates all enqueued facts. If a conflict arises, the conflicting clause is returned,
|    otherwise CRef_Undef.
|  
|    Post-conditions:
|      * the propagation queue is empty, even if there was a conflict.
|________________________________________________________________________________________________@*/
CRef Solver::propagate()
{
    CRef    confl     = CRef_Undef;
    int     num_props = 0;
    watches.cleanAll();

    while (qhead < trail.size()){
        Lit            p   = trail[qhead++];     // 'p' is enqueued fact to propagate.
        vec<Watcher>&  ws  = watches[p];
        Watcher        *i, *j, *end;
        num_props++;

        for (i = j = (Watcher*)ws, end = i + ws.size();  i != end;){
            // Try to avoid inspecting the clause:
            Lit blocker = i->blocker;
            if (value(blocker) == l_True){
                *j++ = *i++; continue; }

            // Make sure the false literal is data[1]:
            CRef     cr        = i->cref;
            Clause&  c         = ca[cr];
            Lit      false_lit = ~p;
            if (c[0] == false_lit)
                c[0] = c[1], c[1] = false_lit;
            assert(c[1] == false_lit);
            i++;

            // If 0th watch is true, then clause is already satisfied.
            Lit     first = c[0];
            Watcher w     = Watcher(cr, first);
            if (first != blocker && value(first) == l_True){
                *j++ = w; continue; }

            // Look for new watch:
            for (int k = 2; k < c.size(); k++)
                if (value(c[k]) != l_False){
                    c[1] = c[k]; c[k] = false_lit;
                    watches[~c[1]].push(w);
                    goto NextClause; }

            // Did not find watch -- clause is unit under assignment:
            *j++ = w;
            //togasaki
            if(c.getImported()){//use import clause
            	c.setUse(c.getUse() + 1);
            	w_imp_cla_use_cnt++;
            }else if(c.learnt() && !c.getImported()){
            	w_own_cla_use_cnt++;
            }else if (!c.learnt()){
            	w_original_cla_use_cnt++;
            }

            if (value(first) == l_False){
                confl = cr;
                qhead = trail.size();
                // Copy the remaining watches:
                while (i < end)
                    *j++ = *i++;
            }else
                uncheckedEnqueue(first, cr);

        NextClause:;
        }
        ws.shrink(i - j);
    }
    propagations += num_props;
    simpDB_props -= num_props;

    return confl;
}


/*_________________________________________________________________________________________________
|
|  reduceDB : ()  ->  [void]
|  
|  Description:
|    Remove half of the learnt clauses, minus the clauses locked by the current assignment. Locked
|    clauses are clauses that are reason to some assignment. Binary clauses are never removed.
|________________________________________________________________________________________________@*/
struct reduceDB_lt { 
    ClauseAllocator& ca;
    reduceDB_lt(ClauseAllocator& ca_) : ca(ca_) {}
    bool operator () (CRef x, CRef y) {
    	//x < y 0
    	//x > y 1
    	//TT added by togasaki
    	// Main criteria... Like in MiniSat we keep all binary clauses
    	if(ca[x].size()> 2 && ca[y].size()==2) return 1;

    	if(ca[y].size()>2 && ca[x].size()==2) return 0;
    	if(ca[x].size()==2 && ca[y].size()==2) return 0;

    	// Second one  based on literal block distance
//    	if(ca[x].lbd()> ca[y].lbd()) return 1;
//    	if(ca[x].lbd()< ca[y].lbd()) return 0;

    	return ca[x].activity() < ca[y].activity();
    }
};
void Solver::reduceDB()
{
	//togasaki
	w_reduceDb_cnt++;//cnt

    int     i, j;
    double  extra_lim = cla_inc / learnts.size();    // Remove any clause below this activity

    sort(learnts, reduceDB_lt(ca));
    // Don't delete binary or locked clauses. From the rest, delete clauses from the first half
    // and clauses with activity smaller than 'extra_lim':
    for (i = j = 0; i < learnts.size(); i++){
        Clause& c = ca[learnts[i]];
	//	assert(c.lbd() > 0);
	//	assert(!c.getImported());
        if (c.size() > 2 && !locked(c) && (i < learnts.size() / 2 || c.activity() < extra_lim)){
        	if(c.getImported()){
        		if(c.getUse() <= 4){
        			w_rm_imp_cla_use_cnt[c.getUse()]++;
        		}else{
        			w_rm_other_imp_cla_use_cnt++;
        		}
        	}
        	removeClause(learnts[i]);
        }else{
            learnts[j++] = learnts[i];
        }
    }
    learnts.shrink(i - j);
    checkGarbage();

 //   std::cout << my_id << " " << w_unused_imp_cla_use_cnt << std::endl;
}


void Solver::removeSatisfied(vec<CRef>& cs)
{
    int i, j;
    for (i = j = 0; i < cs.size(); i++){
        Clause& c = ca[cs[i]];
        if (satisfied(c))
            removeClause(cs[i]);
        else
            cs[j++] = cs[i];
    }
    cs.shrink(i - j);
}


void Solver::rebuildOrderHeap()
{
    vec<Var> vs;
    for (Var v = 0; v < nVars(); v++)
        if (decision[v] && value(v) == l_Undef)
            vs.push(v);
    order_heap.build(vs);
}


/*_________________________________________________________________________________________________
|
|  simplify : [void]  ->  [bool]
|  
|  Description:
|    Simplify the clause database according to the current top-level assigment. Currently, the only
|    thing done here is the removal of satisfied clauses, but more things can be put here.
|________________________________________________________________________________________________@*/
bool Solver::simplify()
{
    assert(decisionLevel() == 0);

    if (!ok || propagate() != CRef_Undef)
        return ok = false;

    if (nAssigns() == simpDB_assigns || (simpDB_props > 0))
        return true;

    // Remove satisfied clauses:
    removeSatisfied(learnts);
    if (remove_satisfied)        // Can be turned off.
        removeSatisfied(clauses);
    checkGarbage();
    rebuildOrderHeap();

    simpDB_assigns = nAssigns();
    simpDB_props   = clauses_literals + learnts_literals;   // (shouldn't depend on stats really, but it will do for now)

    return true;
}
/*
  Finite subsequences of the Luby-sequence:

  0: 1
  1: 1 1 2
  2: 1 1 2 1 1 2 4
  3: 1 1 2 1 1 2 4 1 1 2 1 1 2 4 8
  ...


 */

static double luby(double y, int x){

    // Find the finite subsequence that contains index 'x', and the
    // size of that subsequence:
    int size, seq;
    for (size = 1, seq = 0; size < x+1; seq++, size = 2*size+1);

    while (size-1 != x){
        size = (size-1)>>1;
        seq--;
        x = x % size;
    }

    return pow(y, seq);
}

/*_________________________________________________________________________________________________
|
|  search : (nof_conflicts : int) (params : const SearchParams&)  ->  [lbool]
|  
|  Description:
|    Search for a model the specified number of conflicts. 
|    NOTE! Use negative value for 'nof_conflicts' indicate infinity.
|  
|  Output:
|    'l_True' if a partial assigment that is consistent with respect to the clauseset is found. If
|    all variables are decision variables, this means that the clause set is satisfiable. 'l_False'
|    if the clause set is unsatisfiable. 'l_Undef' if the bound on number of conflicts is reached.
|________________________________________________________________________________________________@*/
lbool Solver::search(int nof_conflicts, int curr_restarts, double &rest_base)
{
    assert(ok);
    int         backtrack_level;
    int         conflictC = 0;
    vec<Lit>    learnt_clause;

    starts++;
    double start,end;
    //togasaki
    std::vector<int> vector_clause;
    uint32_t hit;
    for (;;){
    	start = realTime();
    	pthread_mutex_lock(&mutex);
    	end = realTime();
    	w_search_lock_time += (end - start);
    	bool tmp = terminate_search;
    	pthread_mutex_unlock(&mutex);
    	if (tmp){
    		return l_Undef;
    	}

        CRef confl = propagate();
        if (confl != CRef_Undef){
            // CONFLICT
            conflicts++; conflictC++;
            if (decisionLevel() == 0) return l_False;

            learnt_clause.clear();
            analyze(confl, learnt_clause, backtrack_level);
            cancelUntil(backtrack_level);
            //togasaki
            w_sum_learnt_size += learnt_clause.size();

	    //togasaki
            if (learnt_clause.size() == 1){
            	ConvertLitsClauseVectorClause(learnt_clause, vector_clause);//sort
            	vector_clause.emplace_back(1);//set LBD
            	vector_clause.emplace_back(my_id);//set myId
            	addExportLearntDb(vector_clause);
            	//togasaki
                uncheckedEnqueue(learnt_clause[0]);
            }else{
                CRef cr = ca.alloc(learnt_clause, true);
                learnts.push(cr);
                attachClause(cr);
                claBumpActivity(ca[cr]);
                uncheckedEnqueue(learnt_clause[0], cr);

        		//LBD
                //togasaki
				unsigned int nblevels = computeLBD(learnt_clause);//compute LBD
				ca[cr].setLBD(nblevels);
				ca[cr].setId(my_id);

				//export learnt clause
				 if (learnt_clause.size() <= effective_limit_size || learnt_clause.size() <= w_share_limit_size){
				   ConvertLitsClauseVectorClause(learnt_clause, vector_clause);//sort
				   ca[cr].setExmported(1);
				   if (learnt_clause.size() <= w_share_limit_size){
				     InsertWorkerTable(vector_clause);
				   }
				   vector_clause.emplace_back(nblevels);//set LBD
				   vector_clause.emplace_back(my_id);//set id
					 //std::cout << my_id << " " << nblevels << std::endl;
				   addExportLearntDb(vector_clause);
					 //set exported
				 }

            }

            varDecayActivity();
            claDecayActivity();

            if (--learntsize_adjust_cnt == 0){
                learntsize_adjust_confl *= learntsize_adjust_inc;
                learntsize_adjust_cnt    = (int)learntsize_adjust_confl;
                max_learnts             *= learntsize_inc;

                if (verbosity >= 1){
                    printf("|%02d | %9d | %7d %8d %8d | %8d %8d %6.0f | %6.3f %% |\n",
                    		(int)my_id,
							(int)conflicts,
							(int)dec_vars - (trail_lim.size() == 0 ? trail.size() : trail_lim[0]), nClauses(), (int)clauses_literals,
							(int)max_learnts, nLearnts(), (double)learnts_literals/nLearnts(), progressEstimate()*100);
                }
                //ShowExtraLearntClauseDb();
                //showLearntDb();
            }

        }else{

            // NO CONFLICT
            if (nof_conflicts >= 0 && conflictC >= nof_conflicts || !withinBudget()){
                // Reached bound on number of conflicts:
                progress_estimate = progressEstimate();
                rest_base = luby_restart ? luby(restart_inc, curr_restarts) : pow(restart_inc, curr_restarts);
                cancelUntil(0);
                //togasaki
                return l_Undef;
            }

            // Simplify the set of problem clauses:
            if (decisionLevel() == 0 && !simplify())
                return l_False;

            //minisat2.20 based
            if (!w_linear_reduce_db && learnts.size()-nAssigns() >= max_learnts){
                // Reduce the set of learnt clauses:
                reduceDB();
            }

            //mordern sat solver based
            if (w_linear_reduce_db && conflicts >= curRestart * nbclausesbeforereduce){
            	assert(learnts.size()>0);
            	curRestart = (conflicts/ nbclausesbeforereduce)+1;
            	reduceDB();
            	nbclausesbeforereduce += incReduceDB;
            }

            Lit next = lit_Undef;
            while (decisionLevel() < assumptions.size()){
                // Perform user provided assumption:
                Lit p = assumptions[decisionLevel()];
                if (value(p) == l_True){
                    // Dummy decision level:
                    newDecisionLevel();
                }else if (value(p) == l_False){
                    analyzeFinal(~p, conflict);
                    return l_False;
                }else{
                    next = p;
                    break;
                }
            }

            if (next == lit_Undef){
                // New variable decision:
                decisions++;
                next = pickBranchLit();

                if (next == lit_Undef)
                    // Model found:
                    return l_True;
            }

            // Increase decision level and enqueue 'next'
            newDecisionLevel();
            uncheckedEnqueue(next);
        }
    }
}


double Solver::progressEstimate() const
{
    double  progress = 0;
    double  F = 1.0 / nVars();

    for (int i = 0; i <= decisionLevel(); i++){
        int beg = i == 0 ? 0 : trail_lim[i - 1];
        int end = i == decisionLevel() ? trail.size() : trail_lim[i];
        progress += pow(F, i) * (end - beg);
    }

    return progress / nVars();
}



// NOTE: assumptions passed in member-variable 'assumptions'.
lbool Solver::solve_()
{
    model.clear();
    conflict.clear();
    if (!ok) return l_False;

    solves++;

    max_learnts               = nClauses() * learntsize_factor;
    learntsize_adjust_confl   = learntsize_adjust_start_confl;
    learntsize_adjust_cnt     = (int)learntsize_adjust_confl;
    lbool   status            = l_Undef;

//    if (verbosity >= 1){
//        printf("============================[ Search Statistics ]==============================\n");
//        printf("| Conflicts |          ORIGINAL         |          LEARNT          | Progress |\n");
//        printf("|           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |\n");
//        printf("===============================================================================\n");
//    }

    // Search:
    int curr_restarts = 0;
    double start,end;
    //togasaki
    restart_first = (my_id + 1) * 5;//diversity
    double rest_base = luby_restart ? luby(restart_inc, curr_restarts) : pow(restart_inc, curr_restarts);
     while (status == l_Undef){
        status = search(rest_base * restart_first, curr_restarts, rest_base);
        if (!withinBudget()) break;
        curr_restarts++;
        start = realTime();
        pthread_mutex_lock(&mutex);
        end = realTime();
        w_search_lock_time += (end - start);
        if (status == l_True || status == l_False){
        	find_solution = true;
        	solution_status = (status == l_True ? 0 : 1);
        }
        bool finish = terminate_search;
        if (finish){
            pthread_mutex_unlock(&mutex);
        	return l_Undef;
        }
        if (status != l_Undef){
            pthread_mutex_unlock(&mutex);
            break;
        }
        pthread_mutex_unlock(&mutex);
        	if (imp_learnt_clause){
        		start = realTime();
        		status = importLearntFromQue();
        		end = realTime();
        		w_imp_from_que_time += (end - start);

        	}
		if (curr_restarts % sp_bump_interval == 0){
			start = realTime();
			//SpecialBumpingRestart();
			end = realTime();
			w_sp_bump_restart_time += (end - start);
		}
        	start = realTime();
        	pthread_mutex_lock(&mutex);
        	end = realTime();
        	w_search_lock_time += (end - start);
        	if (status == l_True || status == l_False){
        		find_solution = true;
        		solution_status = (status == l_True ? 0 : 1);
        	}
        	pthread_mutex_unlock(&mutex);
     }

    if (status == l_True){
        // Extend & copy model:
        model.growTo(nVars());
        for (int i = 0; i < nVars(); i++) model[i] = value(i);
    }else if (status == l_False && conflict.size() == 0)
        ok = false;

    cancelUntil(0);
    return status;
}

//=================================================================================================
// Writing CNF to DIMACS:
// 
// FIXME: this needs to be rewritten completely.

static Var mapVar(Var x, vec<Var>& map, Var& max)
{
    if (map.size() <= x || map[x] == -1){
        map.growTo(x+1, -1);
        map[x] = max++;
    }
    return map[x];
}


void Solver::toDimacs(FILE* f, Clause& c, vec<Var>& map, Var& max)
{
    if (satisfied(c)) return;

    for (int i = 0; i < c.size(); i++)
        if (value(c[i]) != l_False)
            fprintf(f, "%s%d ", sign(c[i]) ? "-" : "", mapVar(var(c[i]), map, max)+1);
    fprintf(f, "0\n");
}


void Solver::toDimacs(const char *file, const vec<Lit>& assumps)
{
    FILE* f = fopen(file, "wr");
    if (f == NULL)
        fprintf(stderr, "could not open file %s\n", file), exit(1);
    toDimacs(f, assumps);
    fclose(f);
}


void Solver::toDimacs(FILE* f, const vec<Lit>& assumps)
{
    // Handle case when solver is in contradictory state:
    if (!ok){
        fprintf(f, "p cnf 1 2\n1 0\n-1 0\n");
        return; }

    vec<Var> map; Var max = 0;

    // Cannot use removeClauses here because it is not safe
    // to deallocate them at this point. Could be improved.
    int cnt = 0;
    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]]))
            cnt++;
        
    for (int i = 0; i < clauses.size(); i++)
        if (!satisfied(ca[clauses[i]])){
            Clause& c = ca[clauses[i]];
            for (int j = 0; j < c.size(); j++)
                if (value(c[j]) != l_False)
                    mapVar(var(c[j]), map, max);
        }

    // Assumptions are added as unit clauses:
    cnt += assumptions.size();

    fprintf(f, "p cnf %d %d\n", max, cnt);

    for (int i = 0; i < assumptions.size(); i++){
        assert(value(assumptions[i]) != l_False);
        fprintf(f, "%s%d 0\n", sign(assumptions[i]) ? "-" : "", mapVar(var(assumptions[i]), map, max)+1);
    }

    for (int i = 0; i < clauses.size(); i++)
        toDimacs(f, ca[clauses[i]], map, max);

    if (verbosity > 0)
        printf("Wrote %d clauses with %d variables.\n", cnt, max);
}


//=================================================================================================
// Garbage Collection methods:

void Solver::relocAll(ClauseAllocator& to)
{
    // All watchers:
    //
    // for (int i = 0; i < watches.size(); i++)
    watches.cleanAll();
    for (int v = 0; v < nVars(); v++)
        for (int s = 0; s < 2; s++){
            Lit p = mkLit(v, s);
            // printf(" >>> RELOCING: %s%d\n", sign(p)?"-":"", var(p)+1);
            vec<Watcher>& ws = watches[p];
            for (int j = 0; j < ws.size(); j++)
                ca.reloc(ws[j].cref, to);
        }

    // All reasons:
    //
    for (int i = 0; i < trail.size(); i++){
        Var v = var(trail[i]);
        if (reason(v) != CRef_Undef && (ca[reason(v)].reloced() || locked(ca[reason(v)])))
            ca.reloc(vardata[v].reason, to);
    }

    // All learnt:
    //
    for (int i = 0; i < learnts.size(); i++)
        ca.reloc(learnts[i], to);

    // All original:
    //
    for (int i = 0; i < clauses.size(); i++)
        ca.reloc(clauses[i], to);
}


void Solver::garbageCollect()
{
    // Initialize the next region to a size corresponding to the estimated utilization degree. This
    // is not precise but should avoid some unnecessary reallocations for the new region:
    ClauseAllocator to(ca.size() - ca.wasted()); 

    relocAll(to);
    if (verbosity >= 2)
        printf("|  Garbage collection:   %12d bytes => %12d bytes             |\n", 
               ca.size()*ClauseAllocator::Unit_Size, to.size()*ClauseAllocator::Unit_Size);
    to.moveTo(ca);
}
//togasaki LBD
inline uint32_t Solver::computeLBD(const Clause &c) {
  int nblevels = 0;
  FLAG_LBD++;
   // -------- DEFAULT MODE. NOT A LOT OF DIFFERENCES... BUT EASIER TO READ
    for(int i=0;i<c.size();i++) {
      int l = level(var(c[i]));
      if (FLAG_diff[l] !=FLAG_LBD) {
    	  FLAG_diff[l] = FLAG_LBD;
    	  nblevels++;
      }
    }
    return nblevels;
}
/*Caluculate LBD*/
inline unsigned int Solver::computeLBD(const vec<Lit> & lits) {
	int nblevels = 0;
	FLAG_LBD++;
	// -------- DEFAULT MODE. NOT A LOT OF DIFFERENCES... BUT EASIER TO READ
	for(int i=0;i<lits.size();i++) {
		int l = level(var(lits[i]));
		if (FLAG_diff[l] !=FLAG_LBD) {
			FLAG_diff[l] = FLAG_LBD;
			nblevels++;
		}
	}
	return nblevels;

}
////togasaki parallel
void Solver::addExportLearntDb(const std::vector<int>& vector_learnt){

	pthread_mutex_lock(&mutex);
	export_learnt_worker_db.emplace(vector_learnt);
	ts_learnt_clause.emplace(realTime());//sec
	w_exp_clause++;

	pthread_mutex_unlock(&mutex);
	return ;
}

lbool Solver::addExtraClause(const std::vector<int> &extra_clause){
	int size = extra_clause.size() - 2;
	vec<Lit> lits;
	int imp_lbd = extra_clause[extra_clause.size() - 2];
	int imp_id = extra_clause[extra_clause.size() - 1];
	Lit q        = lit_Undef;

	for(int i=0; i < size; i++) {
		Lit q = toLit(extra_clause[i]);
		if(value(q) == l_False && level(var(q)) == 0)
			continue;
		if (value(q) == l_True){

			return l_Undef;
		}
		lits.push(q);
	}
	//conflict clause at level 0
	if(lits.size() == 0){
		return l_False;
	}
	// case of unit extra clause
	else if(lits.size() == 1){
		w_imp_clause++;
		cancelUntil(0);
		if(value(lits[0]) == l_Undef)uncheckedEnqueue(lits[0]);
	}else {//share

		std::vector<int> tmp;
		tmp.resize(lits.size());
		for (int i = 0; i < lits.size(); i++){
			tmp[i] = lits[i].x;
			//import clause bump
//			varBumpActivity(var(lits[i]));
		}
		if (w_clause_table.count(tmp) > 0){
			return l_Undef;
		}
		InsertWorkerTable(tmp);

		// build clause from lits
		w_imp_clause++;
		CRef cr = ca.alloc(lits, true);
		learnts.push(cr);
		attachClause(cr);
		claBumpActivity(ca[cr]);

		//parallel
		ca[cr].setImported(1);
		ca[cr].setId(imp_id);
		ca[cr].setLBD(imp_lbd);
		//std::cout << imp_id << " " << imp_lbd << std::endl;
	}

	return l_Undef;
}


lbool Solver::importLearntFromQue(){
	pthread_mutex_lock(&mutex);
	//vec<Lit>  extra_clause;
	while (!w_import_learnt_que.empty()){//while que is empty
		std::vector<int> &tmp = w_import_learnt_que.front() ;
		w_all_imp_clause++;
		lbool status = addExtraClause(tmp);
		w_import_learnt_que.pop();
		if (status != l_Undef){
			pthread_mutex_unlock(&mutex);
			return status;
		}
	}
	pthread_mutex_unlock(&mutex);

	return l_Undef;
}
void Solver::ConvertLitsClauseVectorClause(const vec<Lit> &learnt, std::vector<int> &vector_clause){//sort
	vector_clause.clear();
	vector_clause.resize(learnt.size());
	for (int i = 0; i < learnt.size(); i++){
		vector_clause[i] = learnt[i].x;
	}
	std::sort(vector_clause.begin(), vector_clause.end());
	return ;
}
void Solver::InsertWorkerTable(const std::vector<int> &learnt){
	w_clause_table.insert(learnt);
	return ;
}

double Solver::realTime(){
	std::chrono::system_clock::time_point tmp = std::chrono::system_clock::now() ;
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tmp - real_time).count() / 1000.0;

	return elapsed;
}

//special bump
inline void Solver::varSpecialDecayActivity(){
  w_sp_var_inc *= 1 / w_sp_var_decay;
  return ;
}
inline void Solver::varSpecialBumpActivity(int  v) { 
  varSpecialBumpActivity(v, w_sp_var_inc); 
 return ;
}
inline void Solver::varSpecialBumpActivity(int v, double inc) {
    if ( (w_sp_activity[v] += inc) > 1e100 ) {
        // Rescale:
        for (int i = 0; i < nVars(); i++){
            w_sp_activity[i] *= 1e-100;
        }
        w_sp_var_inc *= 1e-100;
    }

    // Update order_heap with respect to new activity:
    if (w_sp_order_heap.inHeap(v)){
       w_sp_order_heap.decrease(v);
    }

    return ;
}


void Solver::SpecialBumpingRestart(){

	for (int i = 0; i < nVars() / 10; i++){
		int var = w_rnd() % nVars();
		varBumpActivity(var, 10000 * var_inc);
	}
  return ;
}
