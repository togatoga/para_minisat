/*****************************************************************************************[Main.cc]
Copyright (c) 2003-2006, Niklas Een, Niklas Sorensson
Copyright (c) 2007,      Niklas Sorensson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT
OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **************************************************************************************************/
#include <errno.h>
#include <signal.h>
#include <zlib.h>
#include <sys/resource.h>

#include "utils/System.h"
#include "utils/ParseUtils.h"
#include "utils/Options.h"
#include "core/Dimacs.h"
#include "simp/SimpSolver.h"

/*Togasaki*/
#include "mpi.h"
#include "utils/Communication.h"
#include "pthread.h"
#include <unistd.h>
#include <chrono>
#include <string>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace Minisat;

//=================================================================================================
static Solver* solver;
std::chrono::system_clock::time_point initial_time;
std::string file_name;

std::string parseFileName(const char *name){
	std::string file_name = "";
	for (int i = strlen(name) - 1; i >= 0; i--){
		if (name[i] == '/'){
			break;
		}
		file_name += name[i];
	}
	std::reverse(file_name.begin(), file_name.end());
	file_name += "_" + std::to_string(solver->num_procs);
	//file_name += "half-share-hybrid";
	if (solver->imp_learnt_clause){
		file_name += "_share";
	}else{
		file_name += "_no-share";
	}

	if (solver->w_hit_restart){
		file_name += "_hit-restart";
		file_name += "_eff-lim-size" + std::to_string(solver->effective_limit_size);
		//file_name += "_" + std::to_string(solver->w_hit_rate);
	}else{
		file_name += "_no-hit-restart";
	}

	return file_name;
}
double realTime(){
	std::chrono::system_clock::time_point tmp = std::chrono::system_clock::now() ;
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tmp - initial_time).count() / 1000.0;
	return elapsed;
}

double getWTime(void){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return ((double)(tv.tv_sec) + (double)(tv.tv_usec) * 1e-6);
}



void printParallelStatistics(Solver& solver){

	std::string ts_learnt_clause_file_name = file_name;
	ts_learnt_clause_file_name += "_paralearnt.txt";
		//timestamp learnt clause
	std::ofstream writing_file;
	writing_file.open(ts_learnt_clause_file_name.c_str(), std::ios::out);
	for (const auto &ts : solver.check_dup_clause){
		//learnt clause worker id
		if (ts.first.size() > 2)continue;
		uint64_t bit = solver.m_clause_table_worker_id[ts.first];
		for (int worker = 1; worker < solver.num_procs; worker++){
			if (bit & (1ULL << worker)){
				writing_file << worker << " ";
			}
		}
		writing_file << std::endl;
		//time
		writing_file << ts.second << std::endl;

		//learnt clause
		for (const auto &var : ts.first){
			writing_file << var << " ";
		}
		writing_file << std::endl;
	}
	uint64_t m_imp_all_clause = 0;
	for (int i = 1; i < solver.num_procs; i++){
		m_imp_all_clause += solver.m_sum_getlearntclause[i];
	}
	writing_file.close();
	//import statistics
	std::string para_status_name = file_name;
	para_status_name += "_para.txt";
	writing_file.open(para_status_name.c_str(), std::ios::out);

	writing_file << "Import_ALL_Learnt_Clause_Num " << m_imp_all_clause << std::endl;
	writing_file << "Export_ALL_Learnt_Clause_Num " << solver.m_exp_clause << std::endl;
	writing_file << "Learnt_Clause_Table_Num " << solver.check_dup_clause.size() << std::endl;
	writing_file << "Erase_Learnt_Clause_Num " << solver.m_erase_master_db_num << std::endl;
	writing_file.close();
	return;
}
void printStats(Solver& solver)
{
	double real_time = realTime();
	double mem_used = memUsedPeak();

	printf("--------------------------------------------Winner!!!!!!!!!!!!!!!-------------------------------------------\n");
	//printf("Winner\t[%02d]\n",solver.winner_node);
	printf("restarts\t[%"PRIu64"]\n", solver.starts);
	printf("conflicts\t[%-12"PRIu64"]   (%.0f /sec)\n", solver.conflicts   , solver.conflicts   /real_time);
	printf("decisions\t[%-12"PRIu64"]   (%4.2f %% random) (%.0f /sec)\n", solver.decisions, (float)solver.rnd_decisions*100 / (float)solver.decisions, solver.decisions   /real_time);
	printf("propagations\t[%-12"PRIu64"]   (%.0f /sec)\n", solver.propagations, solver.propagations/real_time);
	printf("conflict literals\t[%-12"PRIu64"]   (%4.2f %% deleted)\n", solver.tot_literals, (solver.max_literals - solver.tot_literals)*100 / (double)solver.max_literals);
	if (mem_used != 0) printf("Memory used\t[%.2f MB]\n", mem_used);
	printf("CPU time\t[%g s]\n", real_time);

	return ;
}



void printStats(int node, Solver& solver, bool winner)
{
	double real_time = realTime();
	double mem_used = memUsedPeak();
	if (solver.verbosity){
		if (winner){
			printf("Winner!!!!!!!!!!!!!!!-------------------------------------------\n");
			printf("Winner %02d\n",node);
		}else{
			printf("Loser!!!!!!!!!!!!!------------------------------------------------\n");
			printf("Loser %02d\n",node);
		}
		printf("Restarts %u\n", solver.m_starts[node]);
		printf("Dynamic_Restarts %u\n", solver.m_w_dynamic_start[node]);
		if (solver.m_conflicts[node] != 0){
			printf("Hit_Average(w_hit_sum/w_num_effective_clause) %.010lf\n", (double)solver.m_w_sum_hit[node] / solver.m_conflicts[node]);
		}else{
			printf("Hit_Average(w_hit_sum/w_num_effective_clause) %.010lf\n", -1.0);
		}

		printf("Learnt_Clause_Table %u\n", solver.m_w_clause_table_size[node]);
		printf("Effective_Learnt_Clause %u\n", solver.m_w_num_effective_clause[node]);
		printf("Conflicts %u\n", solver.m_conflicts[node]);
		//reduceDb_Num
		printf("ReduceDb_Num %u\n", solver.m_w_reduceDb_cnt[node]);
		if (solver.m_conflicts[node] != 0){
			printf("Learnt_Average_Size %.03lf\n", solver.m_w_sum_learnt_size[node] / solver.m_conflicts[node]);
		}else{
			printf("Learnt_Average_Size %.03lf\n", -1.0);
		}
		printf("Decisions %u\n", solver.m_decisions[node]);
		printf("Propagations %u\n", solver.m_propagations[node]);
		printf("Conflict_literals %u \n", solver.m_tot_literals[node]);
		if (mem_used != 0) printf("Memory_used(MB) %.2f\n", solver.m_w_mem_used[node]);
		//parallel
		printf("PutLearntClause_time(sec) %.12lf\n", solver.m_w_put_cla_time[node]);
		printf("RecvLearntClause_time(sec) %.12lf\n", solver.m_w_recv_cla_time[node]);
		printf("ImportLearntFromQue_time(sec) %.12lf\n", solver.m_w_imp_from_que_time[node]);
		printf("Special_Bumping_Restart(sec) %.12lf\n", solver.m_w_sp_bump_restart_time[node]);

		printf("Search_lock_time(sec) %.12lf\n", solver.m_w_search_lock_time[node]);
		printf("Import_ALL_Learnt_Clause %u\n", solver.m_w_all_imp_clause[node]);
		printf("Import_Learnt_Clause %u\n", solver.m_w_imp_clause[node]);
		printf("Export_Learnt_Clause %u\n", solver.m_w_exp_clause[node]);
		//use counter
		printf("Original_Clause_Use %"PRIu64"\n", solver.m_w_original_cla_use_cnt[node]);
		printf("Own_Learnt_Use %"PRIu64"\n", solver.m_w_own_cla_use_cnt[node]);
		printf("Import_Learnt_Use %"PRIu64"\n", solver.m_w_imp_cla_use_cnt[node]);

		for (int i = 0; i < 5; i++){
			printf("Remove_Learnt_Clause_%d_cnt %u\n", i, solver.m_w_rm_imp_cla_use_cnt[node][i]);
		}
		printf("Remove_Learnt_Clause_Other_cnt %u\n",  solver.m_w_rm_other_imp_cla_use_cnt[node]);

		if (solver.m_sum_getlearntclause[node] != 0){
			printf("Duplication %.12lf\n", solver.duplicated_ratio[node] * 1. / solver.m_sum_getlearntclause[node]);
		}else{
			printf("Duplication %.12lf\n",  -1.0);
		}
		printf("Real_time(sec) %.3lf\n", real_time);
	}
	//write file
	std::string status_file_name = file_name;
	status_file_name += "_status.txt";
	FILE *status_file = fopen(status_file_name.c_str(), "a	");
	if (winner){
		fprintf(status_file,"c ---Winner!!----\n");
		fprintf(status_file,"Winner %02d\n",node);
	}else{
		fprintf(status_file,"c ---Loser!!---\n");
		fprintf(status_file,"Loser %02d\n",node);
	}
	fprintf(status_file,"Restarts %u\n", solver.m_starts[node]);
	fprintf(status_file,"Dynamic_Restarts %u\n", solver.m_w_dynamic_start[node]);

	if (solver.m_conflicts[node] != 0){
		fprintf(status_file, "Hit_Average(w_hit_sum/w_num_effective_clause) %.010lf\n", (double)solver.m_w_sum_hit[node] / solver.m_conflicts[node]);
	}else{
		fprintf(status_file, "Hit_Average(w_hit_sum/w_num_effective_clause) %.010lf\n", -1.0);
	}
	fprintf(status_file, "Learnt_Clause_Table %u\n", solver.m_w_clause_table_size[node]);
	fprintf(status_file,"Effective_Learnt_Clause %u\n", solver.m_w_num_effective_clause[node]);
	fprintf(status_file,"Conflicts %u\n", solver.m_conflicts[node]);

	fprintf(status_file,"ReduceDb_Num %u\n", solver.m_w_reduceDb_cnt[node]);
	if (solver.m_conflicts[node] != 0){
		fprintf(status_file,"Learnt_Average_Size %.03lf\n", solver.m_w_sum_learnt_size[node] / solver.m_conflicts[node]);
	}else{
		fprintf(status_file, "Learnt_Average_Size %.03lf\n", -1.0);
	}
	fprintf(status_file,"Decisions %u\n", solver.m_decisions[node]);
	fprintf(status_file,"Propagations %u\n", solver.m_propagations[node]);
	fprintf(status_file,"Conflict_literals %u \n", solver.m_tot_literals[node]);
	if (mem_used != 0) fprintf(status_file,"Memory_used(MB) %.2f\n", solver.m_w_mem_used[node]);
	//parallel
	fprintf(status_file,"PutLearntClause_time(sec) %.12lf\n", solver.m_w_put_cla_time[node]);
	fprintf(status_file,"RecvLearntClause_time(sec) %.12lf\n", solver.m_w_recv_cla_time[node]);
	fprintf(status_file,"ImportLearntFromQue_time(sec) %.12lf\n", solver.m_w_imp_from_que_time[node]);
	fprintf(status_file, "Special_Bumping_Restart(sec) %.12lf\n", solver.m_w_sp_bump_restart_time[node]);

	fprintf(status_file,"Search_lock_time(sec) %.12lf\n", solver.m_w_search_lock_time[node]);
	fprintf(status_file,"Import_ALL_Learnt_Clause %u\n", solver.m_w_all_imp_clause[node]);
	fprintf(status_file,"Import_Learnt_Clause %u\n", solver.m_w_imp_clause[node]);
	fprintf(status_file,"Export_Learnt_Clause %u\n", solver.m_w_exp_clause[node]);
	//use counter
	fprintf(status_file, "Original_Clause_Use %"PRIu64"\n", solver.m_w_original_cla_use_cnt[node]);
	fprintf(status_file, "Own_Learnt_Use %"PRIu64"\n", solver.m_w_own_cla_use_cnt[node]);
	fprintf(status_file, "Import_Learnt_Use %"PRIu64"\n", solver.m_w_imp_cla_use_cnt[node]);

	for (int i = 0; i < 5; i++){
		fprintf(status_file, "Remove_Learnt_Clause_%d_Num %u\n", i, solver.m_w_rm_imp_cla_use_cnt[node][i]);
	}
	fprintf(status_file, "Remove_Learnt_Clause_Other_Num %u\n",  solver.m_w_rm_other_imp_cla_use_cnt[node]);

	if (solver.m_sum_getlearntclause[node] != 0){
		fprintf(status_file,"Duplication %.12lf\n", solver.duplicated_ratio[node] * 1. / solver.m_sum_getlearntclause[node]);
	}else{
		fprintf(status_file,"Duplication %.12lf\n", -1.0);
	}

	fprintf(status_file,"Real_time(sec) %.3lf\n", real_time);

	fclose(status_file);
	return ;
}

void printMasterStats(Solver& solver){
	return ;
}


//static Solver* solver;
// Terminate by notifying the solver and back out gracefully. This is mainly to have a test-case
// for this feature of the Solver as it may take longer than an immediate call to '_exit()'.
static void SIGINT_interrupt(int signum) { solver->interrupt(); }

// Note that '_exit()' rather than 'exit()' has to be used. The reason is that 'exit()' calls
// destructors and may cause deadlocks if a malloc/free function happens to be running (these
// functions are guarded by locks for multithreaded use).
static void SIGINT_exit(int signum) {
	printf("\n"); printf("*** INTERRUPTED ***\n");
	if (solver->verbosity > 0){
		printStats(*solver);
		printf("\n"); printf("*** INTERRUPTED ***\n"); }
	_exit(1); }


//parallel
int myMasterNode(int my_id, int numprocs){
	return 0;
}

bool isMaster(int my_id){
	if (my_id == 0){
		return true;
	}
	return false;
}

bool CheckFindASolution(){

	pthread_mutex_lock(&solver->mutex);
	bool flag = solver->find_solution;
	pthread_mutex_unlock(&solver->mutex);
	return flag;
}




void *CommunicateWithMaster(void *param){
	bool terminate = false;
	double start,end;
	while (true){
		if (terminate)break;
		EraseImportMasterDb(solver);
		char recv_msg = RecvMsg(solver->master_node);
		switch (recv_msg) {
			case 'P'://Polling
				if (CheckFindASolution()){
					SendMsg(solver->master_node, 'F');
				}else{
					SendMsg(solver->master_node, 'N');
				}
				break;
			case 'C'://Learnt → Master
				PutLearntClause(solver->master_node, solver);
				break;
			case 'S'://Master → worker
				RecvLearntClause(solver->master_node, solver);
				break;
			case 'W'://winner
				SendResult(solver->master_node, solver, true);
				break;
			case 'L' ://loser
				SendResult(solver->master_node, solver, false);
				break;
			case 'T'://terminate find a solution
				terminate = true;
				pthread_mutex_lock(&solver->mutex);
				solver->terminate_search = true;
				pthread_mutex_unlock(&solver->mutex);
				break;
			default:
				break;
		}

	}
	return NULL;
	//std::cout << " Worker communication is end" << std::endl;
}




	//=================================================================================================
	// Main:

	int main(int argc, char** argv)
	{
		try {
			int my_id , num_procs;
			MPI_Init(&argc , &argv);
			MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
			MPI_Comm_size(MPI_COMM_WORLD, &num_procs);




			if (my_id == 0){
				printf(" ---------------------------------------------------------------------------------------\n");
				printf("|                                 marysat2.0    %d process                                 |\n", num_procs);
				printf(" ----------------------------------------------------------------------------------------\n");
			}

			//setUsageHelp("USAGE: %s [options] <input-file> <result-output-file>\n\n  where input may be either in plain or gzipped DIMACS.\n");
			// printf("This is MiniSat 2.0 beta\n");

#if defined(__linux__)
			fpu_control_t oldcw, newcw;
			_FPU_GETCW(oldcw); newcw = (oldcw & ~_FPU_EXTENDED) | _FPU_DOUBLE; _FPU_SETCW(newcw);
			//printf("WARNING: for repeatability, setting FPU to use double precision\n"); nazo by togasaki
#endif
			// Extra options:
			//
			IntOption    verb   ("MAIN", "verb",   "Verbosity level (0=silent, 1=some, 2=more).", 1, IntRange(0, 2));
			BoolOption   pre    ("MAIN", "pre",    "Completely turn on/off any preprocessing.", true);
			StringOption dimacs ("MAIN", "dimacs", "If given, stop after preprocessing and write the result to this file.");
			IntOption    real_lim("MAIN", "real-lim","Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
			IntOption    cpu_lim("MAIN", "cpu-lim","Limit on CPU time allowed in seconds.\n", INT32_MAX, IntRange(0, INT32_MAX));
			IntOption    mem_lim("MAIN", "mem-lim","Limit on memory usage in megabytes.\n", INT32_MAX, IntRange(0, INT32_MAX));

			parseOptions(argc, argv, true);

			SimpSolver  S;
			initial_time = std::chrono::system_clock::now();
			//std::cout << "initial_time = " << initial_time << std::endl;
			if (!pre) S.eliminate(true);
			//std::cout << "num_procs = " << num_procs << std::endl;
			S.verbosity = verb;
			//parallel togasaki
			S.random_seed = my_id;
			S.my_id = my_id;
			S.num_procs = num_procs;
			S.master_node = myMasterNode(my_id, num_procs);

			S.w_rnd.seed(my_id);
			pthread_t handle;
			pthread_mutex_init(&S.mutex, NULL);
			S.initParallelStatus();
			solver = &S;

			// Use signal handlers that forcibly quit until the solver will be able to respond to
			// interrupts:
			signal(SIGINT, SIGINT_exit);
			signal(SIGXCPU,SIGINT_exit);

			// Set limit on CPU-time:
			if (cpu_lim != INT32_MAX){
				rlimit rl;
				getrlimit(RLIMIT_CPU, &rl);
				if (rl.rlim_max == RLIM_INFINITY || (rlim_t)cpu_lim < rl.rlim_max){
					rl.rlim_cur = cpu_lim;
					if (setrlimit(RLIMIT_CPU, &rl) == -1)
						printf("WARNING! Could not set resource limit: CPU-time.\n");
				} }

			// Set limit on virtual memory:
			if (mem_lim != INT32_MAX){
				rlim_t new_mem_lim = (rlim_t)mem_lim * 1024*1024;
				rlimit rl;
				getrlimit(RLIMIT_AS, &rl);
				if (rl.rlim_max == RLIM_INFINITY || new_mem_lim < rl.rlim_max){
					rl.rlim_cur = new_mem_lim;
					if (setrlimit(RLIMIT_AS, &rl) == -1)
						printf("WARNING! Could not set resource limit: Virtual memory.\n");
				} }

			if (argc == 1)
				printf("Reading from standard input... Use '--help' for help.\n");

			gzFile in = (argc == 1) ? gzdopen(0, "rb") : gzopen(argv[1], "rb");
			if (my_id == 0){
				file_name = parseFileName(argv[1]);
			}
			if (in == NULL)
				printf("ERROR! Could not open file: %s\n", argc == 1 ? "<stdin>" : argv[1]), exit(1);

			if (isMaster(my_id) and S.verbosity > 0){
				printf("============================[ Problem Statistics ]=============================\n");
				printf("|                                                                             |\n"); }

			parse_DIMACS(in, S);
			gzclose(in);
			FILE* res = (argc >= 3) ? fopen(argv[2], "wb") : NULL;

			if (isMaster(my_id) and S.verbosity > 0){
				printf("|  Number of variables:  %12d                                         |\n", S.nVars());
				printf("|  Number of clauses:    %12d                                         |\n", S.nClauses()); }

			std::chrono::system_clock::time_point parsed_time = std::chrono::system_clock::now();
			if (isMaster(my_id) and S.verbosity > 0)
				printf("|  Parse time:           %12.2f s                                       |\n", std::chrono::duration_cast<std::chrono::milliseconds>(parsed_time - initial_time).count() / 1000.0);

			// Change to signal-handlers that will only notify the solver and allow it to terminate
			// voluntarily:
			signal(SIGINT, SIGINT_interrupt);
			signal(SIGXCPU,SIGINT_interrupt);

			S.eliminate(true);
			std::chrono::system_clock::time_point  simplified_time = std::chrono::system_clock::now(); ;
			if (isMaster(my_id) and S.verbosity > 0){
				printf("|  Simplification time:  %12.2f s                                       |\n", std::chrono::duration_cast<std::chrono::milliseconds>(simplified_time - initial_time).count() / 1000.0);
				printf("|                                                                             |\n"); }

			if (!S.okay()){
				if (isMaster(my_id)){
					if (res != NULL) fprintf(res, "UNSAT\n"), fclose(res);
					if (S.verbosity > 0){
						printf("===============================================================================\n");
						printf("Solved by simplification\n");
						printStats(S);
						printf("\n"); }
					printf("UNSATISFIABLE\n");
				}
				MPI_Finalize();
				return 0;
			}
			if (isMaster(my_id) && S.verbosity >= 1){
				printf("============================[ Search Statistics ]==============================\n");
				printf("|ID|  Conflicts |          ORIGINAL         |          LEARNT          | Progress |\n");
				printf("|  |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |\n");
				printf("===============================================================================\n");
			}
			if (dimacs){
				if (isMaster(my_id) and S.verbosity > 0)
					printf("==============================[ Writing DIMACS ]===============================\n");
				S.toDimacs((const char*)dimacs);
				if (isMaster(my_id) and S.verbosity > 0)
					printStats(S);
				exit(0);
			}


			lbool ret = l_Undef;
			//master

			if (isMaster(my_id)){
				CommunicateWithWorker(num_procs, real_lim, solver);
			}
			//worker
			if (!isMaster(my_id)){
				pthread_create(&handle, NULL, CommunicateWithMaster, (void *)NULL);
				vec<Lit> dummy;
				ret = S.solveLimited(dummy);

			}
			if (!isMaster(my_id)){
				pthread_join(handle, NULL);
			}
			MPI_Barrier(MPI_COMM_WORLD);
			if (isMaster(my_id)){
				if (S.winner_node != -1){
					printStats(S.winner_node, S, true);//print winner solver status
				}
				for (int node = 1; node < num_procs; node++){
					if (node != S.winner_node){
						printStats(node, S, false);//print loser solver status
					}
				}
				printParallelStatistics(S);

				ret = S.solution_status == 0 ? l_True : S.solution_status == 1 ? l_False : l_Undef;
				printf(ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");
				//write file
				std::string result_file_name = file_name;
				result_file_name += "_result.txt";
				FILE *result_file = fopen(result_file_name.c_str(), "w");
				fprintf(result_file, ret == l_True ? "SATISFIABLE\n" : ret == l_False ? "UNSATISFIABLE\n" : "INDETERMINATE\n");
				fclose(result_file);
				if (res != NULL){
					if (ret == l_True){
						fprintf(res, "SAT\n");
						for (int i = 0; i < S.nVars(); i++)
							if (S.model[i] != l_Undef)
								fprintf(res, "%s%s%d", (i==0)?"":" ", (S.model[i]==l_True)?"":"-", i+1);
						fprintf(res, " 0\n");
					}else if (ret == l_False)
						fprintf(res, "UNSAT\n");
					else
						fprintf(res, "INDET\n");
				}
			}

			MPI_Barrier(MPI_COMM_WORLD);
			MPI_Finalize();
			return 0;
#ifdef NDEBUG
			exit(ret == l_True ? 10 : ret == l_False ? 20 : 0);     // (faster than "return", which will invoke the destructor for 'Solver')
#else
			return (ret == l_True ? 10 : ret == l_False ? 20 : 0);
#endif
		} catch (OutOfMemoryException&){
			printf("===============================================================================\n");
			printf("INDETERMINATE\n");
			exit(0);
		}
	}
